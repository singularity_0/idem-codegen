# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_security_group.cluster-node:
  aws.ec2.security_group.present:
  - resource_id: {{ params.get("aws_security_group.cluster-node")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster-node
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz-cluster-node"},
      {"Key": "kubernetes.io/cluster/"+params.get("clusterName"), "Value": "owned"},
      {"Key": "cluster", "Value": "xyz"}, {"Key": "role", "Value": "xyz-worker"}]}}
  - description: Security group for all nodes in the cluster

aws_security_group.cluster-node-rule-0:
  aws.ec2.security_group_rule.present:
  - name: sgr-06fa8980bf9bffea7
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-0")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: false
  - ip_protocol: -1
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      nessus scanner
  - referenced_group_info:
      GroupId: ${aws.ec2.security_group:aws_security_group.nessus_vuln_scanner:resource_id}
      UserId: 123456789012

aws_security_group.cluster-node-rule-1:
  aws.ec2.security_group_rule.present:
  - name: sgr-0746837a711b2f632
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-1")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1025
  - to_port: 65535
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: ${aws.ec2.security_group:aws_security_group.cluster:resource_id}
      UserId: 123456789012

aws_security_group.cluster-node-rule-2:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a410f396195a1a29
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-2")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: true
  - ip_protocol: -1
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []

aws_security_group.cluster-node-rule-3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0b6d618ed84f17b83
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-3")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 10.170.0.0/16
  - tags: []
  - description: Allow bastion to communicate with worker nodes

aws_security_group.cluster-node-rule-4:
  aws.ec2.security_group_rule.present:
  - name: sgr-0d08ef367d62e82f3
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-4")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: ${aws.ec2.security_group:aws_security_group.cluster:resource_id}
      UserId: 123456789012

aws_security_group.cluster-node-rule-5:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ffb22f7ca7a0f16c
  - resource_id: {{ params.get("aws_security_group.cluster-node-rule-5")}}
  - group_id: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
  - is_egress: false
  - ip_protocol: -1
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow node to communicate with each other
  - referenced_group_info:
      GroupId: ${aws.ec2.security_group:aws_security_group.cluster-node:resource_id}
      UserId: 123456789012
