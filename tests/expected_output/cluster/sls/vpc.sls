aws_cloudwatch_log_group.redlock_flow_log_group:
  aws.cloudwatch.log_group.present:
  - name: xyz-{{ params.get("clusterName") }}_redlock_flow_log_group
  - resource_id: {{ params.get("aws_cloudwatch_log_group.redlock_flow_log_group")}}
  - arn: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group:*
  - tags: {{ params.get("local_tags_dict") }}

aws_db_subnet_group.db-subnet-group:
  aws.rds.db_subnet_group.present:
  - db_subnet_group_arn: arn:aws:rds:eu-west-3:123456789012:subgrp:db-subnet-group-idem-test
    db_subnet_group_description: For Aurora rds
    name: db-subnet-group-{{ params.get("clusterName") }}
    resource_id: {{ params.get("aws_db_subnet_group.db-subnet-group")}}
    subnets:
    - ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
    tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-db-subnet-group"}]}}

{% for i in range(3) %}
aws_eip.nat_eip-{{i}}:
  aws.ec2.elastic_ip.present:
  - name: {{ params.get("aws_eip.nat_eip-"+(i | string))}}
  - resource_id: {{ params.get("aws_eip.nat_eip-"+(i | string))}}
  - allocation_id: eipalloc-0134ceb9112c887fd
  - domain: vpc
  - network_border_group: {{params.get("region")}}
  - public_ipv4_pool: amazon
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-natgw-eip-"+(i
      | string)"}]}}
{% endfor %}

# ToDo: The attribute subnet_ids has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
# ToDo: The attribute subnet_ids has conditional operator. Use {% if params.get(variable_name) %} and {% else %} for if-else condition
aws_elasticache_subnet_group.default:
  aws.elasticache.cache_subnet_group.present:
  - name: elasticache-subnet-group-{{ params.get("clusterName") }}
  - resource_id: {{ params.get("aws_elasticache_subnet_group.default")}}
  - cache_subnet_group_description: For elastcache redis cluster
  - arn: arn:aws:elasticache:eu-west-3:123456789012:subnetgroup:elasticache-subnet-group-idem-test
  - subnet_ids:
    - ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
    - ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
  - tags: []

aws_flow_log.redlock_flow_log:
  aws.ec2.flow_log.present:
  - resource_ids:
    - ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - resource_type: VPC
  - resource_id: {{ params.get("aws_flow_log.redlock_flow_log")}}
  - iam_role: ${aws.iam.role:aws_iam_role.redlock_flow_role:arn}
  - log_group_name: ${aws.cloudwatch.log_group:aws_cloudwatch_log_group.redlock_flow_log_group:resource_id}
  - traffic_type: ALL
  - log_destination_type: cloud-watch-logs
  - log_destination: arn:aws:logs:eu-west-3:123456789012:log-group:xyz-idem-test_redlock_flow_log_group
  - log_format: ${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport}
      ${dstport} ${protocol} ${packets} ${bytes} ${start} ${end} ${action} ${log-status}
  - max_aggregation_interval: 600
  - tags: []

aws_iam_role.redlock_flow_role:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.redlock_flow_role")}}
  - name: xyz-{{ params.get("clusterName") }}_redlock_flow_role
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test_redlock_flow_role
  - id: AROAX2FJ77DC7NUDJJ6DH
  - path: /
  - max_session_duration: 3600
  - tags: {{ params.get("local_tags") }}
  - assume_role_policy_document: {"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "vpc-flow-logs.amazonaws.com"}, "Sid": ""}],
      "Version": "2012-10-17"}

aws_iam_role_policy.redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: {{ params.get("aws_iam_role_policy.redlock_flow_policy")}}
  - role_name: ${aws.iam.role:aws_iam_role.redlock_flow_role:resource_id}
  - name: xyz-{{ params.get("clusterName") }}_redlock_flow_policy
  - policy_document: {"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}

# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_internet_gateway.cluster:
  aws.ec2.internet_gateway.present:
  - attachments:
    - State: available
      VpcId: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
    name: igw-0eee9bba485b312a8
    resource_id: {{ params.get("aws_internet_gateway.cluster")}}
    tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz"}]}}
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}

{% for j in range(3) %}
aws_nat_gateway.nat_gateway-{{j}}:
  aws.ec2.nat_gateway.present:
  - name: {{ params.get("aws_nat_gateway.nat_gateway-"+(j | string))}}
  - resource_id: {{ params.get("aws_nat_gateway.nat_gateway-"+(j | string))}}
  - subnet_id: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
  - connectivity_type: public
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-natgw-"+(j
      | string)"}]}}
  - state: available
  - allocation_id: eipalloc-0134ceb9112c887fd
{% endfor %}

{% for k in range(3) %}
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_route_table.cluster-{{k}}:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
    name: {{ params.get("aws_route_table.cluster-"+(k | string))}}
    propagating_vgws: []
    resource_id: {{ params.get("aws_route_table.cluster-"+(k | string))}}
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: ${aws.ec2.nat_gateway:aws_nat_gateway.nat_gateway-0:resource_id}
      Origin: CreateRoute
      State: active
    tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-xyz-private-"+(k
      | string)"}]}}
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
{% endfor %}

{% for l in range(3) %}
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_route_table.xyz_public-{{l}}:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
    name: {{ params.get("aws_route_table.xyz_public-"+(l | string))}}
    propagating_vgws: []
    resource_id: {{ params.get("aws_route_table.xyz_public-"+(l | string))}}
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: ${aws.ec2.internet_gateway:aws_internet_gateway.cluster:resource_id}
      Origin: CreateRoute
      State: active
    tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-xyz-public-"+(l
      | string)"}]}}
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
{% endfor %}

{% for m in range(3) %}
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
# ToDo: The attribute cidr_block has conditional operator. Use {% if params.get(variable_name) %} and {% else %} for if-else condition
# ToDo: The attribute availability_zone has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_subnet.cluster-{{m}}:
  aws.ec2.subnet.present:
  - name: {{ params.get("aws_subnet.cluster-"+(m | string))}}
  - resource_id: {{ params.get("aws_subnet.cluster-"+(m | string))}}
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  {% if params.get("create_vpc")  %}
  - cidr_block: {{ params.get("VpcSuperNet") }}+( (m * 64) | string).0/18
  {% else %}
  - cidr_block: {{ params.get("cluster_pvt_subnet_cidr[m]") }}
  {% endif %}
  - availability_zone: eu-west-3a
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz-node-private"},
      {"Key": "kubernetes.io/cluster/"+params.get("clusterName"), "Value": "shared"},
      {"Key": "kubernetes.io/role/internal-elb", "Value": "1"}]}}
{% endfor %}

{% for n in range(3) %}
# ToDo: The attribute vpc_id has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
# ToDo: The attribute cidr_block has conditional operator. Use {% if params.get(variable_name) %} and {% else %} for if-else condition
# ToDo: The attribute availability_zone has resolved value of data state. Please create a variable with resolved value and use { params.get(variable_name) } instead of resolved value of data state.
aws_subnet.xyz_public_subnet-{{n}}:
  aws.ec2.subnet.present:
  - name: {{ params.get("aws_subnet.xyz_public_subnet-"+(n | string))}}
  - resource_id: {{ params.get("aws_subnet.xyz_public_subnet-"+(n | string))}}
  {% if params.get("create_vpc")  %}
  - vpc_id:  ${aws.ec2.vpc:aws_vpc.cluster-0:resource_id}
  {% else %}
  - vpc_id: ${aws.ec2.vpc:data.aws_vpc.vpc:[0]:resource_id}
  {% endif %}
  {% if params.get("create_vpc")  %}
  - cidr_block: {{ params.get("VpcSuperNet") }}+( (192 + n * 16) | string).0/20
  {% else %}
  - cidr_block: {{ params.get("cluster_public_subnet_cidr[n]") }}
  {% endif %}
  - availability_zone: eu-west-3a
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz-control-public"},
      {"Key": "kubernetes.io/cluster/"+params.get("clusterName"), "Value": "shared"},
      {"Key": "kubernetes.io/role/elb", "Value": "1"}]}}
{% endfor %}

aws_vpc.cluster:
  aws.ec2.vpc.present:
  - name: vpc-0738f2a523f4735bd
  - resource_id: {{ params.get("aws_vpc.cluster")}}
  - instance_tenancy: default
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz-cluster-node"},
      {"Key": "kubernetes.io/cluster/"+params.get("clusterName"), "Value": "shared"}]}}
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-02ab4e7064a606d2b
      CidrBlock: {{ params.get("VpcSuperNet") }}0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: true
  - enable_dns_support: true

aws_vpc_dhcp_options.vpc_options:
  aws.ec2.dhcp_option.present:
  - name: dopt-052512c363a0a800b
  - resource_id: {{ params.get("aws_vpc_dhcp_options.vpc_options")}}
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": "xyz-"+params.get("clusterName")}]}}
  - dhcp_configurations:
    - Key: domain-name
      Values:
      - us-west-2.compute.internal
    - Key: domain-name-servers
      Values:
      - AmazonProvidedDNS

data.aws_ec2_transit_gateway.region-tgw:
  aws.ec2.transit_gateway.search:
  - filters:
    - name: tag:Name
      value:
      - {{ params.get("profile") }}-tgw-{{ params.get("region") }}

data.aws_subnets.public_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
      - name: tags:Name
        value: {{ params.get("public_subnet_name") }}

data.aws_subnets.pvt_subnets:
  exec.run:
  - path: aws.ec2.subnet.list
  - kwargs:
      tags:
      - name: tags:Name
        value: {{ params.get("pvt_subnet_name") }}

data.aws_vpc.vpc:
  aws.ec2.vpc.search:
  - filters:
    - name: tags:Name
      value: {{ params.get("VpcName") }}
