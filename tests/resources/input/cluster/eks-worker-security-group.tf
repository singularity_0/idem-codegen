resource "aws_security_group" "cluster-node" {
  name        = "${var.clusterName}-temp-xyz-cluster-node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.tags,
    {
      "Name"                                     = "${var.clusterName}-temp-xyz-cluster-node"
      "kubernetes.io/cluster/${var.clusterName}" = "owned"
      "cluster"                                  = "xyz"
      "role"                                     = "xyz-worker"
    },
  )
}

output "aws_security_group-cluster-node-id" {
  value = aws_security_group.cluster-node.id
}

resource "aws_security_group_rule" "cluster-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.cluster-node.id
  source_security_group_id = aws_security_group.cluster-node.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = aws_security_group.cluster-node.id
  source_security_group_id = aws_security_group.cluster.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster-node-ingress-cluster-443" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.cluster-node.id
  source_security_group_id = aws_security_group.cluster.id
  to_port                  = 443
  type                     = "ingress"
}

# bastion support
locals {
  bastion_security_group_cidr_all = contains(keys(var.bastion_security_group_cidr),"${var.profile}_${var.region}") ? var.bastion_security_group_cidr : merge(var.bastion_security_group_cidr, {"${var.profile}_${var.region}" = "${var.VpcSuperNet}0.0/16"})
}

resource "aws_security_group_rule" "cluster-ingress-node-ssh" {
  description       = "Allow bastion to communicate with worker nodes"
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.cluster-node.id
  cidr_blocks = [local.bastion_security_group_cidr_all["${var.profile}_${var.region}"]]
  to_port     = 22
  type        = "ingress"
}

# nessus scanner support
resource "aws_security_group_rule" "cluster-node-ingress-nessus" {
  description              = "Allow worker Kubelets and pods to receive communication from the nessus scanner"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.cluster-node.id
  source_security_group_id = aws_security_group.nessus_vuln_scanner.id
  to_port                  = 0
  type                     = "ingress"
}
