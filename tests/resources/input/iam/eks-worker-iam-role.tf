resource "aws_iam_role" "cluster-node" {
  name               = "${var.clusterName}-temp-xyz-cluster-node"
  tags               = local.tags
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

data "aws_vpc" "vpc"{
  tags = {
    Name   = var.VpcName
  }
}

data "aws_subnets" "pvt_subnets" {
  tags = {
    Name  = var.pvt_subnet_name
  }
}

output "aws_iam_role-cluster-node-name" {
  value = aws_iam_role.cluster-node.name
}

output "aws_iam_role-cluster-node-id" {
  value = aws_iam_role.cluster-node.id
}

output "aws_iam_role-cluster-node-arn" {
  value = aws_iam_role.cluster-node.arn
}

resource "aws_iam_role_policy_attachment" "cluster-node-AmazonxyzWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonxyzWorkerNodePolicy"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_role_policy_attachment" "cluster-node-Amazonxyz_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/Amazonxyz_CNI_Policy"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_role_policy_attachment" "cluster-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_role_policy_attachment" "cluster-node-AmazonEC2RoleforSSM" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_role_policy_attachment" "cluster-node-AmazonElasticFileSystemFullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_role_policy_attachment" "cluster-node-AmazonEC2ReadOnlyAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
  role       = aws_iam_role.cluster-node.name
}

resource "aws_iam_instance_profile" "cluster-node" {
  name = "${var.clusterName}-temp-xyz"
  role = aws_iam_role.cluster-node.name
}

output "aws_iam_instance_profile-cluster-node-arn" {
  value = aws_iam_instance_profile.cluster-node.arn
}

output "aws_iam_instance_profile-cluster-node-name" {
  value = aws_iam_instance_profile.cluster-node.name
}

resource "aws_iam_role_policy" "credstash_access_policy" {
  name = "credstash_xyz_${var.clusterName}_access_policy"
  role = aws_iam_role.cluster-node.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Action": [
              "kms:GenerateDataKey",
              "kms:Decrypt"
          ],
          "Effect": "Allow",
          "Resource": "${aws_kms_key.credstash_key.arn}"
      },
      {
          "Action": [
              "dynamodb:PutItem",
              "dynamodb:GetItem",
              "dynamodb:Query",
              "dynamodb:Scan"
          ],
          "Effect": "Allow",
          "Resource": "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.current.account_id}:table/xyz-${var.clusterName}-credential-store"
      }
  ]
}
EOF

}

resource "aws_iam_role_policy" "Amazon_EBS_CSI_Driver" {
  name = "Amazon_EBS_CSI_Driver"
  role = aws_iam_role.cluster-node.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:AttachVolume",
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:CreateVolume",
                "ec2:DeleteSnapshot",
                "ec2:DeleteTags",
                "ec2:DeleteVolume",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeInstances",
                "ec2:DescribeSnapshots",
                "ec2:DescribeTags",
                "ec2:DescribeVolumes",
                "ec2:DescribeVolumesModifications",
                "ec2:DetachVolume",
                "ec2:ModifyVolume"
            ],
            "Resource": "*"
        }
    ]
}
EOF

}
